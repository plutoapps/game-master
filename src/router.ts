
import HomeVue from './components/Home.vue';
import SpellsVue from './components/Spells.vue';
import CharacterVue from './components/Character.vue';
import EncounterVue from './components/Encounter.vue';
import { createRouter, createWebHistory } from 'vue-router';

const history = createWebHistory(import.meta.env.VITE_BASE_PATH);

const routes = [
    { path: '/', component: HomeVue },
    { path: '/spells', component: SpellsVue },
    { path: '/character', component: CharacterVue },
    { path: '/encounter', component: EncounterVue },
]

const router = createRouter({
    history,
    routes,
})

export default router;
