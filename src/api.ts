const apiBasePath = 'https://spicy-political-bovid.glitch.me';

export async function spells(classId: string, classLevel: string) {
    const url = new URL(apiBasePath + '/spells');
    const params = { classId, classLevel };
    url.search = new URLSearchParams(params).toString();
    const response = await fetch(url.toString(), {
        headers: {
            'Sec-Fetch-Mode': 'no-cors'
        }
    });
    return response.json();
}

export async function character(characterId: string) {
    const targetUrl = 'https://character-service.dndbeyond.com/character/v5/character/' + characterId;
    const proxyUrl = new URL(apiBasePath + '/proxy');
    const params = { targetUrl };
    proxyUrl.search = new URLSearchParams(params).toString();
    const response = await fetch(proxyUrl.toString());
    return (await response.json()).data;
}

export async function encounter(encounterId: string) {
    const targetUrl = 'https://encounter-service.dndbeyond.com/v1/encounters/' + encounterId;
    const proxyUrl = new URL(apiBasePath + '/proxy');
    const params = { targetUrl };
    proxyUrl.search = new URLSearchParams(params).toString();
    const response = await fetch(proxyUrl.toString());
    return (await response.json()).data;
}
